Sediment Transport and Erosion Prediction
=========================================

Sediment Transport and Erosion Prediciton (STEP) is an auto bash python script developed by Yanto and Armia Faizal Sababa at the Civil Engineering Department, Jenderal Soedirman University, Purwokerto, Indonesia. This script has been tested and should work on almost all Python 3.6 compilers or higher.


How to Run STEP
===============

## Setup the program
To setup the program, please run the setup file using the command python3 setup.py in the command line or terminal. Please make sure that you are in the folder where the STEP is located.

## Prepare the input
In the input folder, you will find several subfolder containing input file. Please keep the name of the folder and input file as is. To work with in different wateshed, you need to change the content of input file with those of your watershed. Please keep the format of the input file as is. You can modify the value of input file but not the structure.

## Run the script
Once the model input is completely filled, you can run the script using the command python3 STEP.py in the command line or terminal.


Best regards,
Yanto, Ph.D

> Yanto, Ph.D  
> Assistant Professor, Department of Civil Engineering  
> Jenderal Soedirman University  
> Jl. Mayjend Soengkono KM 5 Blater, Purbalingga, Indonesia   
> Email: yanto@unsoed.ac.id  

